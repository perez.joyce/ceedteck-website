---
title: emma g
description: emma g
youtube_link: 'https://www.facebook.com/'
thumbnail: /images/uploads/amen.jpg
author: Admin
---

To see working configuration examples, you can start from a template or check out the CMS demo site. (No login required: click the login button and the CMS will open.) You can refer to the demo configuration code to see how each option was configured.

> You can find details about all configuration options below. Note that YAML syntax allows lists and objects to be written in block or inline style, and the code samples below include a mix of both.

Note: no matter where you access Netlify CMS — whether running locally, in a staging environment, or in your published site — it will always fetch and commit files in your hosted repository (for example, on GitHub), on the branch you configured in your Netlify CMS config.yml file. This means that content fetched in the admin UI will match the content in the repository, which may be different from your locally running site. It also means that content saved using the admin UI will save directly to the hosted repository, even if you're running the UI locally or in staging.

```
https://www.facebook.com/
```
