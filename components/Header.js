import Link from 'next/link'

export default function Header ()
{
    return (
        <>
            <header className="header">
                <nav className="nav">
                    <Link href="/">
                        <a>My Blog</a>
                    </Link>
                    <Link href="/about">
                        <a>About</a>
                    </Link>
                    <Link href="/admin/index.html">
                        <a>Admin</a>
                    </Link>
                </nav>
            </header>
        </>
    )
}